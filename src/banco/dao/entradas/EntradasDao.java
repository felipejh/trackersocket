package banco.dao.entradas;

import model.LogEntradas;

public interface EntradasDao {

	public boolean inserirEntrada(LogEntradas entrada);
}
