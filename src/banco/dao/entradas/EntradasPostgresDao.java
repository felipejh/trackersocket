package banco.dao.entradas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.LogEntradas;

public class EntradasPostgresDao implements EntradasDao {

	private Connection conexao;

	public EntradasPostgresDao(Connection conexao) {
		super();
		this.conexao = conexao;
	}

	@Override
	public boolean inserirEntrada(LogEntradas entrada) {

		PreparedStatement instrucaoSql = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try {

			StringBuilder sql = new StringBuilder();
			sql.append(" INSERT INTO log_entradas ");
			sql.append("    ( ip                  ");
			sql.append("    , latitude            ");
			sql.append("    , longitude           ");
			sql.append("    , qtd_satelites       ");
			sql.append("    , dth_localizacao     ");
			sql.append("    , velocidade          ");
			sql.append("    , direcao             ");
			sql.append("    , mcc                 ");
			sql.append("    , mnc                 ");
			sql.append("    , lac                 ");
			sql.append("    , torre_celular       ");
			sql.append("    , num_seq_serial      ");
			sql.append("    , ind_tempo_real      ");
			sql.append("    , ind_gps_posicionado ");
			sql.append("    , tip_longitude       ");
			sql.append("    , tip_latitude        ");
			sql.append("    , dth_cadastro        ");
			sql.append("    , dth_atualizacao)     ");
			sql.append(" VALUES                   ");
			sql.append(" ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			instrucaoSql = conexao.prepareStatement(sql.toString());

			instrucaoSql.setString(1, entrada.getIp());
			instrucaoSql.setDouble(2, entrada.getLatitude());
			instrucaoSql.setDouble(3, entrada.getLongitude());
			instrucaoSql.setInt(4, entrada.getQtdSatelites());

			String dataLocalizacao = sdf.format(entrada.getDthLocalização());
			instrucaoSql.setTimestamp(5, java.sql.Timestamp.valueOf(dataLocalizacao));

			instrucaoSql.setInt(6, entrada.getVelocidade());
			instrucaoSql.setInt(7, entrada.getDirecao());
			instrucaoSql.setInt(8, entrada.getMcc());
			instrucaoSql.setInt(9, entrada.getMnc());
			instrucaoSql.setInt(10, entrada.getLac());
			instrucaoSql.setInt(11, entrada.getTorreCelular());
			instrucaoSql.setInt(12, entrada.getNumSeqSerial());
			instrucaoSql.setInt(13, entrada.getIndTempoReal());
			instrucaoSql.setInt(14, entrada.getIndGpsPosicionado());
			instrucaoSql.setInt(15, entrada.getTipLongitude());
			instrucaoSql.setInt(16, entrada.getTipLatitude());

			String dthAtual = sdf.format(new Date());
			instrucaoSql.setTimestamp(17, java.sql.Timestamp.valueOf(dthAtual));
			instrucaoSql.setTimestamp(18, java.sql.Timestamp.valueOf(dthAtual));

			instrucaoSql.execute();

			return true;

		} catch (SQLException e) {
			e.printStackTrace();

		}

		return false;
	}

}
