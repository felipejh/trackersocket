package banco.dao.login;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.LogLogin;

public class LoginPostgresDao implements LoginDao{
	
	private Connection conexao;
	
    public LoginPostgresDao(Connection conexao) {
		super();
		this.conexao = conexao;
	}

	@Override
	public boolean inserirLogin(LogLogin login) {
		
		PreparedStatement instrucaoSql = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try {

			StringBuilder sql = new StringBuilder(" INSERT INTO log_login ");
			sql.append(" (imei, ip, num_seq_serial, dth_cadastro, dth_atualizacao) ");
			sql.append(" values ");
			sql.append(" ( ?, ?, ?, ?, ?)");

			instrucaoSql = conexao.prepareStatement(sql.toString());

			instrucaoSql.setLong(1, login.getImei());
			instrucaoSql.setString(2, login.getIp());
			instrucaoSql.setInt(3, login.getNumSeqSerial());
			
			String dthAtual = sdf.format(new Date());
			instrucaoSql.setTimestamp(4, java.sql.Timestamp.valueOf(dthAtual));
			instrucaoSql.setTimestamp(5, java.sql.Timestamp.valueOf(dthAtual));

			//conexao.commit();
			
			instrucaoSql.execute();
			
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			
		} 
		
		return false;

	}
	
}
