package banco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import banco.dao.entradas.EntradasDao;
import banco.dao.entradas.EntradasPostgresDao;
import banco.dao.login.LoginDao;
import banco.dao.login.LoginPostgresDao;
import banco.types.TipoBancoType;

public class BancoDados {

	private static BancoDados INSTANCE;

	// JDBC
	private Connection minhaConexao;
	private TipoBancoType tipoBanco;

	private BancoDados() {
		conectar();
		// controlar pool de conexão
	}

	public static BancoDados getInstance() {
		if (INSTANCE == null)
			INSTANCE = new BancoDados();
		return INSTANCE;
	}

	public void setTipoBanco(TipoBancoType type) {

		this.tipoBanco = type;

	}

	public void conectar() {
		try {

			// Procura a classe do driver
			Class.forName("org.postgresql.Driver");

			// estabelece a conexao com o banco
			minhaConexao = DriverManager.getConnection("jdbc:postgresql://192.168.0.50/rastreador", "tracker",
					"tracker");

			// avisa que conectou com sucesso!
			System.out.println("-----------------------------");
			System.out.println("Conexão efetuada com sucesso!");
			System.out.println("-----------------------------");

			// minhaConexao.close();

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} catch (ClassNotFoundException e) {

			System.out.println("Driver não encontrado!");

		}

	}

	public LoginDao getLoginDao() {

		if (TipoBancoType.POSTGRES.equals(tipoBanco)) {
			return new LoginPostgresDao(minhaConexao);
		
		} else {

			System.out.println("Método de armazenamento não encontrado!");

			return null;
		}
	}
	
	public EntradasDao getEntradasDao() {

		if (TipoBancoType.POSTGRES.equals(tipoBanco)) {
			return new EntradasPostgresDao(minhaConexao);
		
		} else {

			System.out.println("Método de armazenamento não encontrado!");

			return null;
		}
	}

	public void desconectar() {

		try {

			minhaConexao.close();

			// System.out.println("-----------------------------");
			// System.out.println("Conexão desfeita com sucesso!");
			// System.out.println("-----------------------------");

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

}
