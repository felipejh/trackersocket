package encoder;

import decoder.Gt06Decoder;
import protocol.Gt06Protocol;

public class Gt06Encoder implements Gt06Protocol {

	private Gt06Decoder gt06Decoder;
	
	public Gt06Encoder(Gt06Decoder gt06Decoder) {
		super();
		this.gt06Decoder = gt06Decoder;
	}

	@Override
	public String getBitInicial() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTamanhoPacote() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTipInformacao() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNumImei() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getNumSerial() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getErrorCheck() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBitFinal() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String getDataHora() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getLatitude() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Double getLongitude() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getQtdSatelites() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getVelocidade() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getDirecao() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStatusDirecaoBinario() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getIndTempoReal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getIndGpsPosicionado() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getTipLongitude() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getTipLatitude() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMcc() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMnc() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getLac() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getTorreCelular() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getMsgResponseLogin() {
		StringBuilder sb = new StringBuilder();
		sb.append(gt06Decoder.getBitFinal());
		sb.append(gt06Decoder.getTamanhoPacote());
		sb.append(gt06Decoder.getTipInformacao());
		sb.append(gt06Decoder.getNumSerial());
		sb.append(gt06Decoder.getErrorCheck());
		sb.append(gt06Decoder.getBitFinal());
		
		return sb.toString().getBytes();
		
	}

}
