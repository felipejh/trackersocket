package Listener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import banco.BancoDados;
import banco.dao.entradas.EntradasDao;
import banco.types.TipoBancoType;
import decoder.Gt06Decoder;
import ejb.LogsRastreadorEJB;
import encoder.Gt06Encoder;
import model.LogEntradas;
import util.Converter;

public class Gt06Listener {

	private InputStream sockInput = null;
	private OutputStream sockOutput = null;
	private static final Integer PORT_SOCKET = 8090;
	private static final String TIP_LOGIN = "01";
	private static final String TIP_LOCALIZACAO = "12";

	public void startServer() {

		Runnable serverTask = new Runnable() {
			public void run() {

				try {
					@SuppressWarnings("resource")
					ServerSocket serverSocket = new ServerSocket(PORT_SOCKET);
					System.out.println("Aguardando dados do rastreador...");
					while (true) {
						Socket clientSocket = serverSocket.accept();
						sockInput = clientSocket.getInputStream();
						sockOutput = clientSocket.getOutputStream();
						ClientTask ct = new ClientTask(clientSocket);
						new Thread(ct).start();
					}
				} catch (IOException e) {
					System.err.println("Erro na requisi��o");
					e.printStackTrace();
				}
			}
		};
		Thread serverThread = new Thread(serverTask);
		serverThread.start();
	}

	private class ClientTask implements Runnable {

		Socket sk;

		public ClientTask(Socket sk) {
			this.sk = sk;
		}

		public void run() {
			System.out.println("Recebi o primeiro retorno!");
			while (true) {

				byte[] rawMsgFromTracker = new byte[1024];
				int bytes_read = 0;
				try {

					bytes_read = sockInput.read(rawMsgFromTracker);

					if (bytes_read < 0) {
						System.out.println("Erro ao ler dados de entrada do socket.");
					}

					// System.out.println(Converter.byteArrayToHex(rawMsgFromTracker));

					String packet = Converter.byteArrayToHex(rawMsgFromTracker);

					//System.out.println(packet);
					Gt06Decoder gt06 = new Gt06Decoder(packet);

					if (TIP_LOGIN.equals(gt06.getTipInformacao())) {

						System.out.println("Mensagem de login:");

						System.out.println("Bit Inicial : " + gt06.getBitInicial());
						System.out.println("Tamanho do pacote: " + gt06.getTamanhoPacote());
						System.out.println("Tipo informação: " + gt06.getTipInformacao());
						System.out.println("IMEI: " + gt06.getNumImei());
						System.out.println("Serial: " + gt06.getNumSerial());
						System.out.println("Error Check: " + gt06.getErrorCheck());
						System.out.println("Bit final: " + gt06.getBitFinal());

//						LogLogin login = new LogLogin();
//						login.setIp(sk.getInetAddress().toString().substring(1));
//						login.setImei(Long.parseLong(gt06.getNumImei()));
//						login.setNumSeqSerial(Integer.valueOf(gt06.getNumSerial()));
//
//						BancoDados bancoDados = BancoDados.getInstance();
//						bancoDados.setTipoBanco(TipoBancoType.POSTGRES);
//						LoginDao loginDao = bancoDados.getLoginDao();
//						if (loginDao.inserirLogin(login)) {
//							System.out.println("Login inserido com sucesso!");
//						} else {
//							System.out.println("Erro ao inserir login");
//						}

						// Validar se o rastreador está cadastrado no banco

						Gt06Encoder gt06Encoder = new Gt06Encoder(gt06);
						sockOutput.write(gt06Encoder.getMsgResponseLogin());
						sockOutput.flush();

					}

					if (TIP_LOCALIZACAO.equals(gt06.getTipInformacao())) {

						System.out.println("MSG Localização");

						LogEntradas entrada = new LogEntradas();
						entrada.setIp(sk.getInetAddress().toString().substring(1));
						entrada.setLatitude(gt06.getLatitude());
						entrada.setLongitude(gt06.getLongitude());
						entrada.setQtdSatelites(gt06.getQtdSatelites());
						entrada.setVelocidade(gt06.getVelocidade());
						entrada.setDirecao(gt06.getDirecao());
						entrada.setMcc(gt06.getMcc());
						entrada.setMnc(gt06.getMnc());
						entrada.setLac(gt06.getLac());
						entrada.setTorreCelular(gt06.getTorreCelular());
						entrada.setNumSeqSerial(gt06.getNumSerial());
						entrada.setIndTempoReal(gt06.getIndTempoReal());
						entrada.setIndGpsPosicionado(gt06.getIndGpsPosicionado());
						entrada.setTipLongitude(gt06.getTipLongitude());
						entrada.setTipLatitude(gt06.getTipLatitude());

						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Date dtaLocalizacao = sdf.parse(gt06.getDataHora());

						entrada.setDthLocalização(dtaLocalizacao);

						BancoDados bancoDados = BancoDados.getInstance();
						bancoDados.setTipoBanco(TipoBancoType.POSTGRES);
						EntradasDao entradasDao = bancoDados.getEntradasDao();
						if (entradasDao.inserirEntrada(entrada)) {
							System.out.println("LOG ENTRADAS inserido com sucesso!");
						} else {
							System.out.println("Erro ao inserir LOG ENTRADAS");
						}

					}

				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}

		}

	}

}
