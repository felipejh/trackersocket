package protocol;

public interface Gt06Protocol {

	public String getBitInicial();

	public String getTamanhoPacote();
	
	public String getTipInformacao();

	public String getNumImei();

	public Integer getNumSerial();

	public String getErrorCheck();

	public String getBitFinal();
	
	public String getDataHora();
	
	public byte[] getMsgResponseLogin();

	public Double getLatitude();
	
	public Double getLongitude();
	
	public Integer getQtdSatelites();
	
	public Integer getVelocidade();
	
	public Integer getDirecao();
	
	public String getStatusDirecaoBinario();
	
	public Integer getIndTempoReal();
	
	public Integer getIndGpsPosicionado();
	
	public Integer getTipLongitude();
	
	public Integer getTipLatitude();
	
	public Integer getMcc();
	
	public Integer getMnc();
	
	public Integer getLac();
	
	public Integer getTorreCelular();
}
