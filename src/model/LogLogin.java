package model;

public class LogLogin {

	private String ip;
	private Long imei;
	private Integer numSeqSerial;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Long getImei() {
		return imei;
	}

	public void setImei(Long imei) {
		this.imei = imei;
	}

	public Integer getNumSeqSerial() {
		return numSeqSerial;
	}

	public void setNumSeqSerial(Integer numSeqSerial) {
		this.numSeqSerial = numSeqSerial;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((imei == null) ? 0 : imei.hashCode());
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		result = prime * result + ((numSeqSerial == null) ? 0 : numSeqSerial.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogLogin other = (LogLogin) obj;
		if (imei == null) {
			if (other.imei != null)
				return false;
		} else if (!imei.equals(other.imei))
			return false;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (numSeqSerial == null) {
			if (other.numSeqSerial != null)
				return false;
		} else if (!numSeqSerial.equals(other.numSeqSerial))
			return false;
		return true;
	}

}
