package model;

import java.util.Date;

public class LogEntradas {

	private String ip;
	private Double latitude;
	private Double longitude;
	private Integer qtdSatelites;
	private Date dthLocalização;
	private Integer velocidade;
	private Integer direcao;
	private Integer mcc;
	private Integer mnc;
	private Integer lac;
	private Integer torreCelular;
	private Integer numSeqSerial;
	private Integer indTempoReal;
	private Integer indGpsPosicionado;
	private Integer tipLongitude;
	private Integer tipLatitude;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Integer getQtdSatelites() {
		return qtdSatelites;
	}

	public void setQtdSatelites(Integer qtdSatelites) {
		this.qtdSatelites = qtdSatelites;
	}

	public Date getDthLocalização() {
		return dthLocalização;
	}

	public void setDthLocalização(Date dthLocalização) {
		this.dthLocalização = dthLocalização;
	}

	public Integer getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(Integer velocidade) {
		this.velocidade = velocidade;
	}

	public Integer getDirecao() {
		return direcao;
	}

	public void setDirecao(Integer direcao) {
		this.direcao = direcao;
	}

	public Integer getMcc() {
		return mcc;
	}

	public void setMcc(Integer mcc) {
		this.mcc = mcc;
	}

	public Integer getMnc() {
		return mnc;
	}

	public void setMnc(Integer mnc) {
		this.mnc = mnc;
	}

	public Integer getLac() {
		return lac;
	}

	public void setLac(Integer lac) {
		this.lac = lac;
	}

	public Integer getTorreCelular() {
		return torreCelular;
	}

	public void setTorreCelular(Integer torreCelular) {
		this.torreCelular = torreCelular;
	}

	public Integer getNumSeqSerial() {
		return numSeqSerial;
	}

	public void setNumSeqSerial(Integer numSeqSerial) {
		this.numSeqSerial = numSeqSerial;
	}

	public Integer getIndTempoReal() {
		return indTempoReal;
	}

	public void setIndTempoReal(Integer indTempoReal) {
		this.indTempoReal = indTempoReal;
	}

	public Integer getIndGpsPosicionado() {
		return indGpsPosicionado;
	}

	public void setIndGpsPosicionado(Integer indGpsPosicionado) {
		this.indGpsPosicionado = indGpsPosicionado;
	}

	public Integer getTipLongitude() {
		return tipLongitude;
	}

	public void setTipLongitude(Integer tipLongitude) {
		this.tipLongitude = tipLongitude;
	}

	public Integer getTipLatitude() {
		return tipLatitude;
	}

	public void setTipLatitude(Integer tipLatitude) {
		this.tipLatitude = tipLatitude;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((direcao == null) ? 0 : direcao.hashCode());
		result = prime * result + ((dthLocalização == null) ? 0 : dthLocalização.hashCode());
		result = prime * result + ((indGpsPosicionado == null) ? 0 : indGpsPosicionado.hashCode());
		result = prime * result + ((indTempoReal == null) ? 0 : indTempoReal.hashCode());
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		result = prime * result + ((lac == null) ? 0 : lac.hashCode());
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((mcc == null) ? 0 : mcc.hashCode());
		result = prime * result + ((mnc == null) ? 0 : mnc.hashCode());
		result = prime * result + ((numSeqSerial == null) ? 0 : numSeqSerial.hashCode());
		result = prime * result + ((qtdSatelites == null) ? 0 : qtdSatelites.hashCode());
		result = prime * result + ((tipLatitude == null) ? 0 : tipLatitude.hashCode());
		result = prime * result + ((tipLongitude == null) ? 0 : tipLongitude.hashCode());
		result = prime * result + ((torreCelular == null) ? 0 : torreCelular.hashCode());
		result = prime * result + ((velocidade == null) ? 0 : velocidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogEntradas other = (LogEntradas) obj;
		if (direcao == null) {
			if (other.direcao != null)
				return false;
		} else if (!direcao.equals(other.direcao))
			return false;
		if (dthLocalização == null) {
			if (other.dthLocalização != null)
				return false;
		} else if (!dthLocalização.equals(other.dthLocalização))
			return false;
		if (indGpsPosicionado == null) {
			if (other.indGpsPosicionado != null)
				return false;
		} else if (!indGpsPosicionado.equals(other.indGpsPosicionado))
			return false;
		if (indTempoReal == null) {
			if (other.indTempoReal != null)
				return false;
		} else if (!indTempoReal.equals(other.indTempoReal))
			return false;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (lac == null) {
			if (other.lac != null)
				return false;
		} else if (!lac.equals(other.lac))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (mcc == null) {
			if (other.mcc != null)
				return false;
		} else if (!mcc.equals(other.mcc))
			return false;
		if (mnc == null) {
			if (other.mnc != null)
				return false;
		} else if (!mnc.equals(other.mnc))
			return false;
		if (numSeqSerial == null) {
			if (other.numSeqSerial != null)
				return false;
		} else if (!numSeqSerial.equals(other.numSeqSerial))
			return false;
		if (qtdSatelites == null) {
			if (other.qtdSatelites != null)
				return false;
		} else if (!qtdSatelites.equals(other.qtdSatelites))
			return false;
		if (tipLatitude == null) {
			if (other.tipLatitude != null)
				return false;
		} else if (!tipLatitude.equals(other.tipLatitude))
			return false;
		if (tipLongitude == null) {
			if (other.tipLongitude != null)
				return false;
		} else if (!tipLongitude.equals(other.tipLongitude))
			return false;
		if (torreCelular == null) {
			if (other.torreCelular != null)
				return false;
		} else if (!torreCelular.equals(other.torreCelular))
			return false;
		if (velocidade == null) {
			if (other.velocidade != null)
				return false;
		} else if (!velocidade.equals(other.velocidade))
			return false;
		return true;
	}

}
